package com.exalt.villaRentalSystem.service;

import com.exalt.villaRentalSystem.dto.DepartmentDto;
import java.util.List;

public interface DepartmentService {
     List<DepartmentDto> findAll();

     DepartmentDto findById(int id);

     DepartmentDto add(DepartmentDto dep);

     void delete(int id);

}

package com.exalt.villaRentalSystem.service.impl;


import com.exalt.villaRentalSystem.converter.VillaConverter;
import com.exalt.villaRentalSystem.dto.VillaDto;
import com.exalt.villaRentalSystem.dto.mappers.VillaMappers;
import com.exalt.villaRentalSystem.model.Villa;
import com.exalt.villaRentalSystem.projection.VillaProjection;
import com.exalt.villaRentalSystem.repository.VillaRepository;
import com.exalt.villaRentalSystem.service.VillaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;


@Service
public class VillaServiceImpl implements VillaService {

    @Autowired
    private VillaRepository villaRepository;
    @Autowired
    private VillaConverter villaConverter;

    private VillaMappers villaMappers;

    @PostConstruct
    public void init(){
        villaMappers = villaMappers.INSTANCE;
    }
    public void delete(int id) {
      villaRepository.deleteById(id);
    }

    public List<Villa> findVillaByName(String name){
        List<Villa> villas=villaRepository.findVillaByName(name);
        return villas;
    }

    public List<Villa> filterVillasByCost(double cost){
        List<Villa> villas=villaRepository.filterVillasByCost(cost);
        return villas;
    }

    public List<VillaProjection> findAllProjectedBy(){
        return villaRepository.findAllProjectedBy();
    }

    @Override
    public Page<Villa> loadVillasPaging(int page, int size) {
        Pageable pagable =  PageRequest.of(page,size, Sort.Direction.DESC ,"name");
        Page<Villa> villas = villaRepository.findAll(pagable);
        return villas;
    }

    public List<VillaDto> findAll(){
        List<Villa> villas = (List<Villa>) villaRepository.findAll();
        return villaConverter.entityToDto(villas);
    }

    public VillaDto findById(int id){
        Villa villa =villaRepository.findById(id).orElse(null);
        return villaConverter.entityToDto(villa);
    }


    public void save(VillaDto villaDto){
        Villa villa = villaMappers.dtoToVilla(villaDto);
        villaRepository.save(villa);
    }

    public void update(int id , VillaDto villaDto){
        Villa villa =villaRepository.findById(id).orElse(null);
        villa.setName(villaDto.getName());
        villa.setCost(villaDto.getCost());
        villa.setAddress(villaDto.getAddress());
        villa.setDescription(villaDto.getDescription());
        villa.setStatus(villaDto.isStatus());
        villaRepository.save(villa);
    }
}

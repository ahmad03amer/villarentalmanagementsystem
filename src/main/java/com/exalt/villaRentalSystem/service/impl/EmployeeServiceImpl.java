package com.exalt.villaRentalSystem.service.impl;
import com.exalt.villaRentalSystem.converter.EmployeeConverter;
import com.exalt.villaRentalSystem.dto.CustomerDto;
import com.exalt.villaRentalSystem.dto.EmployeeDto;
import com.exalt.villaRentalSystem.model.Customer;
import com.exalt.villaRentalSystem.model.Employee;
import com.exalt.villaRentalSystem.model.Villa;
import com.exalt.villaRentalSystem.projection.EmployeeProjection;
import com.exalt.villaRentalSystem.projection.VillaProjection;
import com.exalt.villaRentalSystem.repository.EmployeeRepository;
import com.exalt.villaRentalSystem.service.EmployeeService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Log
@Service
public  class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    EmployeeConverter employeeConverter;

    public  Employee add(Employee employee) {
        employeeRepository.save(employee);
        return employee;
    }

    public Employee update( Employee employee,int id) {
        Employee updatedEmployee = employeeRepository.findById(id).get();
        updatedEmployee.setId(id);
        updatedEmployee.setFullName(employee.getFullName());
        updatedEmployee.setIdNumber(employee.getIdNumber());
        updatedEmployee.setEmail(employee.getEmail());
        updatedEmployee.setPassword(employee.getPassword());
        updatedEmployee.setPhoneNumber(employee.getPhoneNumber());
        updatedEmployee.setHours(employee.getHours());
        updatedEmployee.setDOB(employee.getDOB());
        updatedEmployee.setRole(employee.getRole());
        updatedEmployee.setGender(employee.getGender());
        updatedEmployee.setSalary(employee.getSalary());
        employeeRepository.save(updatedEmployee);
        return updatedEmployee;
    }

    public void delete(int id) {
        employeeRepository.deleteById(id);
    }

    public List<Employee> filterEmployeeByAddress(@RequestParam String address){
        List<Employee> employees =employeeRepository.filterEmployeeByAddress(address);
        return employees;
    }


    public List<EmployeeProjection> findAllProjectedBy(){
        return employeeRepository.findAllProjectedBy();
    }

    @Override
    public Page<Employee> loadEmployeesPaging(int page, int size) {
        Pageable pagable =  PageRequest.of(page,size, Sort.Direction.DESC ,"fullName");
        Page<Employee> employees = employeeRepository.findAll(pagable);
        return employees;
    }


    public List<EmployeeDto> findAll(){
        List<Employee> employees = employeeRepository.findAll();
        return employeeConverter.entityToDto(employees);
    }

    public EmployeeDto findById(int id){
        Employee employee =employeeRepository.findById(id).orElse(null);
        return employeeConverter.entityToDto(employee);
    }

    public EmployeeDto save(EmployeeDto emp){
        Employee employee = employeeConverter.dtoToEntity(emp);
        employee = employeeRepository.save(employee);
        return employeeConverter.entityToDto(employee);
    }
}

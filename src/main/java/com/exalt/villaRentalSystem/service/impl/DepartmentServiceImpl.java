package com.exalt.villaRentalSystem.service.impl;

import com.exalt.villaRentalSystem.converter.DepartmentConverter;
import com.exalt.villaRentalSystem.dto.DepartmentDto;
import com.exalt.villaRentalSystem.dto.EmployeeDto;
import com.exalt.villaRentalSystem.dto.VillaDto;
import com.exalt.villaRentalSystem.model.Bill;
import com.exalt.villaRentalSystem.model.Department;
import com.exalt.villaRentalSystem.model.Employee;
import com.exalt.villaRentalSystem.model.Villa;
import com.exalt.villaRentalSystem.repository.DepartmentRepository;
import com.exalt.villaRentalSystem.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentServiceImpl implements DepartmentService {
    @Autowired
    DepartmentRepository departmentRepository;
    @Autowired
    DepartmentConverter departmentConverter;


  public List<DepartmentDto> findAll(){
      List<Department> employees = departmentRepository.findAll();
      return departmentConverter.entityToDto(employees);
  }

    public DepartmentDto findById(int id){
        Department department =departmentRepository.findById(id).orElse(null);
        return departmentConverter.entityToDto(department);
    }

    public DepartmentDto add(DepartmentDto dep){
        Department department = departmentConverter.dtoToEntity(dep);
        department = departmentRepository.save(department);
        return departmentConverter.entityToDto(department);
    }

    public void delete(int id) {

        departmentRepository.deleteById(id);
    }

}


package com.exalt.villaRentalSystem.service.impl;
import com.exalt.villaRentalSystem.converter.CustomerConverter;
import com.exalt.villaRentalSystem.dto.CustomerDto;
import com.exalt.villaRentalSystem.dto.VillaDto;
import com.exalt.villaRentalSystem.model.Customer;
import com.exalt.villaRentalSystem.model.Villa;
import com.exalt.villaRentalSystem.projection.CustomerProjection;
import com.exalt.villaRentalSystem.repository.CustomerRepository;
import com.exalt.villaRentalSystem.repository.VillaRepository;
import com.exalt.villaRentalSystem.service.CustomerService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.*;

@Log
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private CustomerConverter customerConverter;
    @Autowired
    private VillaRepository villaRepository;

    public  Customer add(Customer customer) {
        customerRepository.save(customer);
        return customer;
    }

    @Transactional(timeout = 1)
    public Customer update( Customer employee,int id) {
        Customer updatedCustomer = customerRepository.findById(id).get();
        updatedCustomer.setId(id);
        updatedCustomer.setFullName(employee.getFullName());
        updatedCustomer.setIdNumber(employee.getIdNumber());
        updatedCustomer.setEmail(employee.getEmail());
        updatedCustomer.setPassword(employee.getPassword());
        updatedCustomer.setPhoneNumber(employee.getPhoneNumber());
        updatedCustomer.setDOB(employee.getDOB());
        updatedCustomer.setRole(employee.getRole());
        updatedCustomer.setGender(employee.getGender());
        customerRepository.save(updatedCustomer);
        try {
            Thread.sleep(1000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        return updatedCustomer;
    }

    public void delete(int id) {
        customerRepository.deleteById(id);
    }

    public List<Customer> filterCustomersByRole( String role){
        List<Customer> customers =  customerRepository.filterCustomersByRole(role);
        return customers;
    }

    @Transactional(noRollbackFor = NumberFormatException.class)
    public void addVilla(List<Villa> villas, int id){
        Customer customer = customerRepository.findById(id).get();
        customer.setVillas(villas);
        villas.forEach(villa -> villaRepository.save(villa));
    }

    /**
     *
     * @return projected / pagining and dto
     */
    public List<CustomerProjection> findAllProjectedBy(){
        return customerRepository.findAllProjectedBy();
    }

    public Page<Customer> loadEmployeesPaging(int page, int size) {
        Pageable pagable =  PageRequest.of(page,size, Sort.Direction.DESC ,"fullName");
        Page<Customer> customers = customerRepository.findAll(pagable);
        return customers;
    }


    public List<CustomerDto> findAll(){
        List<Customer> customers = customerRepository.findAll();
        return customerConverter.entityToDto(customers);
    }


    public CustomerDto findById(int id){
        Customer customer =customerRepository.findById(id).orElse(null);
        return customerConverter.entityToDto(customer);
    }

    public CustomerDto save(CustomerDto cus){
        Customer customer = customerConverter.dtoToEntity(cus);
        customer = customerRepository.save(customer);
        return customerConverter.entityToDto(customer);
    }

}

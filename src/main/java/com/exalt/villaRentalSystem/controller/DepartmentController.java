package com.exalt.villaRentalSystem.controller;

import com.exalt.villaRentalSystem.converter.DepartmentConverter;
import com.exalt.villaRentalSystem.dto.DepartmentDto;
import com.exalt.villaRentalSystem.dto.VillaDto;
import com.exalt.villaRentalSystem.model.Department;
import com.exalt.villaRentalSystem.repository.EmployeeRepository;
import com.exalt.villaRentalSystem.service.DepartmentService;
import lombok.extern.java.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Log
@RestController
public class DepartmentController {
    Logger log = LoggerFactory.getLogger(VillaController.class);

    @Autowired
    private DepartmentService departmentService ;

        @GetMapping("/v1/dto/departments")
    public List<DepartmentDto> findAll(){
            log.info("user entered the /departments to find all of them");
            List<DepartmentDto> departments = departmentService.findAll();
        return departments;
    }

    @GetMapping("/v1/dto/departments/{id}")
    public DepartmentDto findById(@PathVariable int id){
        log.info("user entered the /departments/id to find an exact department");
        DepartmentDto department = departmentService.findById(id);
        return department;
    }

    @PostMapping("/v1/dto/addDepartment")
    public DepartmentDto add(@ModelAttribute DepartmentDto dep){
        log.info("user entered the addDepartment");
        DepartmentDto department = departmentService.add(dep);
        return department;
    }

    @DeleteMapping("/v1/dto/deleteDep/{id}")
    public void delete( @PathVariable int id){
        log.info("User  entered /deleteDep/{id} to deleteVilla ");
        departmentService.delete(id);
    }

}

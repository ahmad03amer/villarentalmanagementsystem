package com.exalt.villaRentalSystem.controller;
import com.exalt.villaRentalSystem.dto.CustomerDto;
import com.exalt.villaRentalSystem.model.Customer;
import com.exalt.villaRentalSystem.model.Villa;
import com.exalt.villaRentalSystem.projection.CustomerProjection;
import com.exalt.villaRentalSystem.service.CustomerService;
import lombok.extern.java.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Log
@RestController
public class CustomerController {
    Logger log = LoggerFactory.getLogger(VillaController.class);

    @Autowired
    private CustomerService customerService ;

    @GetMapping("/v1/dto/customers")
    public List<CustomerDto> findAll(){
        log.info("user entered the /customers/ to return the all customers");
        List<CustomerDto> customers = customerService.findAll();
        return customers;
    }

    @GetMapping("/v1/dto/customers/{id}")
    public CustomerDto findById(@PathVariable int id){
        log.info("user entered the /customers/id to search a customer");
        CustomerDto customer = customerService.findById(id);
        return customer;
    }

    @PostMapping("/v1/dto/saveCus")
    public CustomerDto save(@ModelAttribute CustomerDto cus){
        log.info("user entered the saveCustomer");
        CustomerDto customer = customerService.save(cus);
        return customer;
    }

    @PostMapping("/v1/customers")
    public Customer  add(@RequestBody  Customer customer){
        log.info("User  entered /customers to addCustomer ");
        customerService.add(customer);
        System.out.println(customer);
        return customer;
    }

    @PutMapping(value = "/v1/customers/{id}")
    public Customer update(@RequestBody Customer customer,@PathVariable int id){
        log.info("User  entered /customers/{id}/ to updateCustomers");
        customerService.update(customer,id);
        return  customer;
    }

    @DeleteMapping(value = "/v1/customers/{id}")
    public void delete( @PathVariable int id){
        log.info("User  entered /customers/{id}/ to deleteCustomers ");
        customerService.delete(id);
    }

    @GetMapping("/v1/customers/filter")
    @ResponseBody
    public List<Customer> filterCustomersByRole(@RequestParam String role){
        log.info("User  entered customers/filter to search for role customers ");
        List<Customer> customers =  customerService.filterCustomersByRole(role);
        return customers;
    }

    @PostMapping("/v1/customers/{id}/villa")
    public List<Villa> addVilla(@ModelAttribute  List<Villa> villas, @PathVariable int id){
        log.info("User  entered /customers to addVilla ");
        customerService.addVilla(villas,id);
        System.out.println(villas);
        return villas;
    }

    @GetMapping("/v1/customers/projectedCus")
    public List<CustomerProjection> findAllProjectedBy(){
        log.info("User  entered /employees to projectedCus ");
        return customerService.findAllProjectedBy();
    }

    @GetMapping("/v1/customers/pagingCus")
    @ResponseBody
    public Page<Customer> loadEmployeesPaging(@RequestParam(name = "page" , defaultValue = "0", required = false) int page
            , @RequestParam(name = "size", defaultValue = "1", required = false) int size){
        log.info("User  entered /employees to pagingCus ");
        return customerService.loadEmployeesPaging(page,size);
    }



}

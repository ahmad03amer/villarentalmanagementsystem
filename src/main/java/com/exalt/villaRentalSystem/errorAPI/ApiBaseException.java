package com.exalt.villaRentalSystem.errorAPI;

import org.springframework.http.HttpStatus;

public abstract class ApiBaseException extends RuntimeException {
    public ApiBaseException(String message) {
        super(message);
    } // TODO: support message and status code

    public abstract HttpStatus getStatusCode();
}